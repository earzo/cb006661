package com.example.earzo.styles.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.models.PurchaseHistory;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListViewAdapterPurchasedItems extends BaseAdapter {

    private Context mContext;
    private List<PurchaseHistory> purchasedItemList;

    public ListViewAdapterPurchasedItems(Context mContext, String UID) {
        this.mContext = mContext;
        purchasedItemList = PurchaseHistory.find(PurchaseHistory.class, "user_ID = ?", UID);
    }

    public int getCount() {
        return purchasedItemList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @TargetApi(21)
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listView;

        if ((convertView == null) && (inflater != null)) {

            // get layout from xml file
            listView = inflater.inflate(R.layout.custom_purchased_list_item, parent, false);
        } else {
            listView = (View) convertView;
        }

        if(listView != null) {
            ConstraintLayout cl = (ConstraintLayout) listView.findViewById(R.id.constraintLayout_purchased);
            TextView textView_purchased_itemName = (TextView) listView.findViewById(R.id.textView_purchased_itemName);
            TextView textView_purchased_orderID = (TextView) listView.findViewById(R.id.textView_purchased_orderID);
            TextView textView_purchased_itemQuantity = (TextView) listView.findViewById(R.id.textView_purchased_itemQuantity);
            TextView textView_purchased_itemPrice = (TextView) listView.findViewById(R.id.textView_purchased_itemPrice);
            TextView textView_purchased_orderTotal = (TextView) listView.findViewById(R.id.textView_purchased_orderTotal);
            ImageView imageView_purchased = (ImageView) listView.findViewById(R.id.imageView_purchased);
            int quantity;
            double price, totalPrice;
            String priceInString, quantityInString, totalPriceInString, orderID;

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                cl.setBackgroundResource(R.drawable.layout_shadow);
            }

            quantity = purchasedItemList.get(position).getQuantity();
            price = purchasedItemList.get(position).getProduct().getPrice();
            totalPrice = (Math.round(price * quantity * 100.0) / 100.0);
            totalPriceInString = "LKR " + Double.toString(totalPrice);
            priceInString = "LKR " + Double.toString(purchasedItemList.get(position).getProduct().getPrice());
            quantityInString = "Quantity " + Integer.toString(quantity);
            orderID = Long.toString(purchasedItemList.get(position).getId());

            textView_purchased_orderID.setText(orderID);
            textView_purchased_itemName.setText(purchasedItemList.get(position).getProduct().getName());
            textView_purchased_itemPrice.setText(priceInString);
            textView_purchased_itemQuantity.setText(quantityInString);
            textView_purchased_orderTotal.setText(totalPriceInString);
            Picasso.get().load(purchasedItemList.get(position).getProduct().getScaledImage()).placeholder(R.drawable.app_launcher_icon).into(imageView_purchased);
        }

        return listView;
    }
}
