package com.example.earzo.styles.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.extra.MailSender;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.User;

public class fragment_changePassword extends Fragment {

    User loggedInUser;
    View view;

    public fragment_changePassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_change_password, container, false);

        final EditText edit_oldPassword = (EditText) view.findViewById(R.id.edit_oldPassword);
        final EditText edit_newPassword = (EditText) view.findViewById(R.id.edit_newPassword);
        final EditText edit_confirmNewPassword = (EditText) view.findViewById(R.id.edit_confirmNewPassword);
        Button button_saveAccountChanges = (Button) view.findViewById(R.id.button_saveAccountChanges);

        UserSession userSession = UserSession.getUserSession(getActivity());
        loggedInUser = User.findById(User.class, userSession.getUID());
        String userName = loggedInUser.getFirstName();
        Long UID = loggedInUser.getId();

        button_saveAccountChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(new EditText[]{edit_oldPassword, edit_newPassword, edit_confirmNewPassword}, edit_oldPassword.getText().toString().trim(), edit_newPassword.getText().toString().trim(), edit_confirmNewPassword.getText().toString().trim());
            }
        });

        return view;
    }

    public void validate(EditText[] userDetails, String oldPassword, String newPassword, String confirmNewPassword) {
        int count = 0;

        for(EditText currentDetail : userDetails){
            if(currentDetail.getText().toString().trim().isEmpty()){
                currentDetail.requestFocus();
                currentDetail.setError(getString(R.string.error_emptyFields));
                count++;
            }
        }


        if(count == 0) {

            if (oldPassword.equals(loggedInUser.getPassword())) {
                if (newPassword.equals(confirmNewPassword)) {
                    loggedInUser.setPassword(newPassword);
                    loggedInUser.save();
                    Toast.makeText(getActivity(), R.string.successful_changesSaved, Toast.LENGTH_SHORT).show();

                    final String body = "Hi " + loggedInUser.getFirstName() + " " + loggedInUser.getLastName() + "," + "\n\nYour password for StyleS by Style Omega™ account has been recently changed.\n\nIf you do not recognize this action, someone might have access to your account.\n\nIf so, please review your account soon to protect your personal data." + "\n\n________________________________________________________\nTeam Style Omega™";

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MailSender sender = new MailSender("styles.noreply@gmail.com", "#StyleS@10#");
                                sender.sendMail("PASSWORD CHANGE in your Style Omega™ account!",
                                        body,
                                        "styles.noreply@gmail.com",
                                        loggedInUser.getUserEmail());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();

                    FragmentManager manager = getActivity().getFragmentManager();
                    manager.popBackStack();

                    FragmentTransaction transaction = getFragmentManager().beginTransaction().remove(this);
                    getActivity().setTitle(R.string.fragmentTitle_manageAccount);
                    Fragment newFragment = new fragment_manageAccount();
                    transaction.replace(R.id.content_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else {
                    Toast.makeText(getActivity(), R.string.error_PWunmatch, Toast.LENGTH_SHORT).show();
                }
            }else{
                EditText edit_oldPassword = (EditText) view.findViewById(R.id.edit_oldPassword);
                edit_oldPassword.requestFocus();
                edit_oldPassword.setError(getString(R.string.error_oldPasswordIsIncorrect));
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        getActivity().setTitle(R.string.fragmentTitle_changePassword);
    }

}
