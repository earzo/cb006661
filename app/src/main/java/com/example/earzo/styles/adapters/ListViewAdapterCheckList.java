package com.example.earzo.styles.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.Product;
import com.example.earzo.styles.models.ShoppingCart;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListViewAdapterCheckList extends BaseAdapter {

    private List<ShoppingCart> shoppingList;
    private Product checkoutProduct;
    private String checkoutStatus;
    private int quantityOfSingleItem; //Quantity needed if there is only one item checkout
    private Context mContext;

    public ListViewAdapterCheckList(Context mContext, @Nullable String checkoutStatus, @Nullable Long PID, int quantityOfSingleItem) {
        this.mContext = mContext;
        this.checkoutStatus = checkoutStatus;
        this.quantityOfSingleItem = quantityOfSingleItem;

        if(checkoutStatus == null){
            UserSession userSession = UserSession.getUserSession(mContext);
            String UID = userSession.getUID().toString();
            shoppingList = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);
        }else{
            checkoutProduct = Product.findById(Product.class, PID);
        }
    }

    public int getCount() {
        if(checkoutStatus == null) {
            return shoppingList.size();
        }

        return 1;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @TargetApi(21)
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listView;

        if ((convertView == null) && (inflater != null)) {

            // get layout from xml file
            listView = inflater.inflate(R.layout.custom_checkout_list_item, parent, false);
        } else {
            listView = (View) convertView;
        }

        if(listView != null) {
            ConstraintLayout cl = (ConstraintLayout) listView.findViewById(R.id.constraintLayout_checkout);
            TextView textView_checkout_itemName = (TextView) listView.findViewById(R.id.textView_checkout_itemName);
            TextView textView_checkout_itemPrice = (TextView) listView.findViewById(R.id.textView_checkout_itemPrice);
            TextView textView_checkout_itemQuantity = (TextView) listView.findViewById(R.id.textView_checkout_itemQuantity);
            ImageView imageView_checkout = (ImageView) listView.findViewById(R.id.imageView_checkout);
            listView.findViewById(R.id.textView_checkout_addMoreItems).setVisibility(View.GONE); //no "Add More Items" link for the final checkout
            String priceInString, quantityInString;

            ConstraintLayout outerCL = (ConstraintLayout)  listView.findViewById(R.id.outerConstraintLayout_checkout);
            //set the bottom constraint of the inner constraint layout with bottom constraint of outer constraint layout
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(outerCL);
            constraintSet.connect(cl.getId(), ConstraintSet.BOTTOM, outerCL.getId(), ConstraintSet.BOTTOM);
            constraintSet.applyTo(outerCL);

            //set the bottom margin of the inner constraint layout to 8dp, so that it'll balance the overall looks
            ConstraintLayout.LayoutParams parameter = (ConstraintLayout.LayoutParams) cl.getLayoutParams();
            parameter.setMargins(parameter.leftMargin, parameter.topMargin, parameter.rightMargin, 20);
            cl.setLayoutParams(parameter);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                cl.setBackgroundResource(R.drawable.layout_shadow);
            }

            if(checkoutStatus == null) {
                listView.findViewById(R.id.imageButton_checkout_delete).setVisibility(View.GONE); //no delete button for the final checkout
                priceInString = "LKR " + Double.toString(shoppingList.get(position).getProduct().getPrice());
                quantityInString = "Quantity " + Integer.toString(shoppingList.get(position).getQuantity());

                textView_checkout_itemName.setText(shoppingList.get(position).getProduct().getName());
                textView_checkout_itemPrice.setText(priceInString);
                textView_checkout_itemQuantity.setText(quantityInString);
                Picasso.get().load(shoppingList.get(position).getProduct().getScaledImage()).placeholder(R.drawable.app_launcher_icon).into(imageView_checkout);

            }else{
                listView.findViewById(R.id.imageButton_checkout_delete).setVisibility(View.GONE); //no delete button for the final checkout
                priceInString = "LKR " + Double.toString(checkoutProduct.getPrice());
                quantityInString = "Quantity " + Integer.toString(quantityOfSingleItem);

                textView_checkout_itemName.setText(checkoutProduct.getName());
                textView_checkout_itemPrice.setText(priceInString);
                textView_checkout_itemQuantity.setText(quantityInString);
                Picasso.get().load(checkoutProduct.getScaledImage()).placeholder(R.drawable.app_launcher_icon).into(imageView_checkout);

            }
        }

        return listView;
    }

}
