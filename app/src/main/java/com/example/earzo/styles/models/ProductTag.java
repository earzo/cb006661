package com.example.earzo.styles.models;

import com.orm.SugarRecord;

public class ProductTag extends SugarRecord {
    private String tag;
    private Product product;

    public ProductTag() {
    }

    public ProductTag(String tag, Product product) {
        this.tag = tag;
        this.product = product;
    }

    public String getTag() {
        return tag;
    }

    public Product getProduct() {
        return product;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
