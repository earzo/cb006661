package com.example.earzo.styles.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.adapters.ListViewAdapterCheckList;
import com.example.earzo.styles.extra.MailSender;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.Product;
import com.example.earzo.styles.models.PurchaseHistory;
import com.example.earzo.styles.models.ShoppingCart;
import com.example.earzo.styles.models.User;

import java.util.List;

public class fragment_checkout extends Fragment {

    String UID;
    Long productID = null;
    int singleItemQuantity = 0;
    List<ShoppingCart> shoppingCartProducts;
    Product singleCheckoutItem;


    public fragment_checkout() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checkout, container, false);

        TextView textView_checkout_numberOfItems = (TextView) view.findViewById(R.id.textView_checkout_numberOfItems);
        TextView textView_checkout_subTotal = (TextView) view.findViewById(R.id.textView_checkout_subTotal);
        TextView textView_checkout_orderTotal = (TextView) view.findViewById(R.id.textView_checkout_orderTotal);
        AppCompatButton button_checkout_finalCheckout = (AppCompatButton) view.findViewById(R.id.button_checkout_finalCheckout);
        ListViewAdapterCheckList listAdapter;
        ListView listview = view.findViewById(R.id.listViewCheckList);

        if(getArguments() != null) {
            productID = getArguments().getLong("PID");
            singleItemQuantity = getArguments().getInt("Quantity");
        }

        UserSession userSession = UserSession.getUserSession(getActivity());
        final User LoggedInUser = User.findById(User.class, userSession.getUID());
        final String email = LoggedInUser.getUserEmail();
        final String fullname = LoggedInUser.getFirstName() + " " + LoggedInUser.getLastName();
        UID = LoggedInUser.getId().toString();

        //if checkout request is via shopping cart
        if(productID == null) {
            listAdapter = new ListViewAdapterCheckList(getActivity(), null, null, 0);
            listview.setAdapter(listAdapter);

            shoppingCartProducts = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);
            int totalNumberOfItems = 0, numberOfItemsPerProduct;
            double subTotal = 0, totalPricePerProduct;

            for (ShoppingCart currentCartProduct : shoppingCartProducts) {
                numberOfItemsPerProduct = currentCartProduct.getQuantity();
                totalPricePerProduct = (Math.round(currentCartProduct.getProduct().getPrice() * numberOfItemsPerProduct) * 100.0) / 100.0;
                subTotal = subTotal + totalPricePerProduct;
                totalNumberOfItems = totalNumberOfItems + numberOfItemsPerProduct;
            }

            String totalQuantityInString = "Items (" + Integer.toString(totalNumberOfItems) + ")";
            String subTotalInString = "LKR " + Double.toString(subTotal);

            textView_checkout_numberOfItems.setText(totalQuantityInString);
            textView_checkout_subTotal.setText(subTotalInString);
            textView_checkout_orderTotal.setText(subTotalInString);
        }
        //if checkout request is from the item directly
        else{
            listAdapter = new ListViewAdapterCheckList(getActivity(), "singleItem", productID, singleItemQuantity);
            listview.setAdapter(listAdapter);

            singleCheckoutItem = Product.findById(Product.class, productID);
            double subTotal = (Math.round(singleCheckoutItem.getPrice() * singleItemQuantity) * 100.0) / 100.0;


            String totalQuantityInString = "Items (" + Integer.toString(singleItemQuantity) + ")";
            String subTotalInString = "LKR " + Double.toString(subTotal);

            textView_checkout_numberOfItems.setText(totalQuantityInString);
            textView_checkout_subTotal.setText(subTotalInString);
            textView_checkout_orderTotal.setText(subTotalInString);

        }

        button_checkout_finalCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(productID == null){

                    StringBuilder fullDetails = new StringBuilder();

                    for(ShoppingCart currentCartItem : shoppingCartProducts){

                        Product product = Product.findById(Product.class, currentCartItem.getProduct().getId());

                        product.setQuantity(product.getQuantity() - currentCartItem.getQuantity());

                        if(product.getQuantity() == 0){
                            product.setActive(false);
                        }

                        product.save(); //update and save the new values for the purchased product

                        new PurchaseHistory(UID, product, currentCartItem.getQuantity()).save();
                        currentCartItem.delete();

                        String itemDetails = "\n\nItem name: " + product.getName() + "\n" +
                                "Item ID: " + product.getId() + "\n" +
                                "Quantity: " + currentCartItem.getQuantity() + "\n" +
                                "Paid: LKR " + (Math.round(product.getPrice() * currentCartItem.getQuantity() * 100.0) / 100.0) + "\n" +
                                "Item image: " + product.getFullImage();

                        fullDetails.append(itemDetails).append("\n\n");
                    }

                    final String subject = "ORDER CONFIRMED!";
                    final String body = "StyleS by Style Omega™\n\nHi " + fullname + "," + "\n\nYour order is confirmed!\n\nReceipt for your purchases:" + fullDetails + "\n\n________________________________________________________\nTHANK YOU FOR THE PURCHASE!";

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MailSender sender = new MailSender("styles.noreply@gmail.com", "#StyleS@10#");
                                sender.sendMail(subject,
                                        body,
                                        "styles.noreply@gmail.com",
                                        email);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    Toast.makeText(getActivity(), R.string.successful_checkout, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), activity_navigationDrawer.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    FragmentManager manager = getActivity().getFragmentManager();
                    manager.popBackStack();
                }else{
                    Product product = Product.findById(Product.class, singleCheckoutItem.getId());
                    product.setQuantity(product.getQuantity() - singleItemQuantity);

                    if(product.getQuantity() == 0){
                        product.setActive(false);
                    }

                    product.save(); //update and save the new values for the purchased product

                    new PurchaseHistory(UID, product, singleItemQuantity).save();

                    final String subject = "ORDER CONFIRMED: " + product.getName();
                    final String itemDetails = "\n\nItem name: " + product.getName() + "\n" +
                            "Item ID: " + product.getId() + "\n" +
                            "Quantity: " + singleItemQuantity + "\n" +
                            "Paid: LKR " + (Math.round(product.getPrice() * singleItemQuantity * 100.0) / 100.0) + "\n" +
                            "Item image: " + product.getFullImage();
                    final String body = "StyleS by Style Omega™\n\nHi " + fullname + "," + "\n\nYour order is confirmed!\n\nReceipt for your purchase:" + itemDetails + "\n\n________________________________________________________\nTHANK YOU FOR THE PURCHASE!";

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MailSender sender = new MailSender("styles.noreply@gmail.com", "#StyleS@10#");
                                sender.sendMail(subject,
                                        body,
                                        "styles.noreply@gmail.com",
                                        email);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    Toast.makeText(getActivity(), R.string.successful_checkout, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), activity_navigationDrawer.class);
                    startActivity(intent);

                    getActivity().finish();
                }
            }
        });

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        if(getActivity() != null) {
            getActivity().setTitle(R.string.fragmentTitle_checkout);
        }
    }

}
