package com.example.earzo.styles.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.extra.MailSender;
import com.example.earzo.styles.models.User;

import java.util.List;

public class activity_forgottenPassword extends activity_base {

    private User user;
    private EditText editText_registeredEmail;
    private EditText editText_forgottenPassword_oldPassword;
    private EditText editText_forgottenPassword_newPassword;
    private EditText editText_forgottenPassword_confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotten_password);

        editText_registeredEmail = (EditText) findViewById(R.id.editText_registeredEmail);
        editText_forgottenPassword_oldPassword = (EditText) findViewById(R.id.editText_forgottenPassword_oldPassword);
        editText_forgottenPassword_newPassword = (EditText) findViewById(R.id.editText_forgottenPassword_newPassword);
        editText_forgottenPassword_confirmPassword = (EditText) findViewById(R.id.editText_forgottenPassword_confirmPassword);
    }

    @Override
    public void onResume(){
        super.onResume();

        this.setTitle(R.string.fragmentTitle_changePassword);
    }

    public void saveChanges(View view){
        List<User> userList = User.find(User.class, "user_email = ?", editText_registeredEmail.getText().toString().trim());

        if(userList.size() == 0){
            editText_registeredEmail.requestFocus();
            editText_registeredEmail.setError(getString(R.string.error_invalidRegisteredEmail));
        }else {
            user = userList.get(0);
            validate(new EditText[]{editText_forgottenPassword_oldPassword, editText_forgottenPassword_newPassword, editText_forgottenPassword_confirmPassword}, editText_forgottenPassword_oldPassword.getText().toString().trim(), editText_forgottenPassword_newPassword.getText().toString().trim(), editText_forgottenPassword_confirmPassword.getText().toString().trim());
        }
    }

    public void validate(EditText[] userDetails, String oldPassword, String newPassword, String confirmNewPassword) {
        int count = 0;

        for(EditText currentDetail : userDetails){
            if(currentDetail.getText().toString().trim().isEmpty()){
                currentDetail.requestFocus();
                currentDetail.setError(getString(R.string.error_emptyFields));
                count++;
            }
        }


        if(count == 0) {

            if (oldPassword.equals(user.getPassword())) {
                if (newPassword.equals(confirmNewPassword)) {
                    user.setPassword(newPassword);
                    user.save();
                    Toast.makeText(this, R.string.successful_changesSaved, Toast.LENGTH_SHORT).show();

                    final String body = "Hi " + user.getFirstName() + " " + user.getLastName() + "," + "\n\nYour password for StyleS by Style Omega™ account has been recently changed.\n\nIf you do not recognize this action, someone might have access to your account.\n\nIf so, please review your account soon to protect your personal data." + "\n\n________________________________________________________\nTeam Style Omega™";

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MailSender sender = new MailSender("styles.noreply@gmail.com", "#StyleS@10#");
                                sender.sendMail("PASSWORD CHANGE in your Style Omega™ account!",
                                        body,
                                        "styles.noreply@gmail.com",
                                        user.getUserEmail());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();

                    Intent intent = new Intent(this, activity_login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    this.finish();
                } else {
                    Toast.makeText(this, R.string.error_PWunmatch, Toast.LENGTH_SHORT).show();
                }
            }else{
                EditText editText_forgottenPassword_oldPassword = (EditText) findViewById(R.id.editText_forgottenPassword_oldPassword);
                editText_forgottenPassword_oldPassword.requestFocus();
                editText_forgottenPassword_oldPassword.setError(getString(R.string.error_oldPasswordIsIncorrect));
            }
        }
    }

}
