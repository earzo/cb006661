package com.example.earzo.styles.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.List;

public class Product extends SugarRecord implements Parcelable {
    private String name;
    private String shortDescription;
    private String longDescription;
    private double price;
    private int quantity;
    private boolean active;
    private String scaledImage;
    private String fullImage;
    @Ignore
    private List<Integer> tag;
    private String tagList;
    private Integer integer;

    public Product() {
    }

    private Product(Parcel in){
        this.integer = in.readInt();
        this.tag = new ArrayList<>();
        in.readList(tag, Integer.class.getClassLoader());
    }

    public static final Creator<Product> CREATOR = new Creator<Product>(){
      public Product createFromParcel(Parcel in){
          return new Product(in);
      }
      public Product[] newArray(int size){
          return new Product[size];
      }
    };

    @Override
    public long save(){
        this.tagList = new Gson().toJson(tag);
        return super.save();
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags){
        destination.writeString(tagList);
        destination.writeList(getTag());
    }

    public void setTag(ArrayList<Integer> tag) {
        this.tag = tag;
    }

    public List<Integer> getTag() {
        tag = new Gson().fromJson(this.tagList, new TypeToken<List<Integer>>(){}.getType());
        return tag;
    }

    public String getName() {
        return name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean isActive() {
        return active;
    }

    public String getScaledImage() {
        return scaledImage;
    }

    public String getFullImage() {
        return fullImage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setScaledImage(String scaledImage) {
        this.scaledImage = scaledImage;
    }

    public void setFullImage(String fullImage) {
        this.fullImage = fullImage;
    }
}
