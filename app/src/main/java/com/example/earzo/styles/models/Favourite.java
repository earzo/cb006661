package com.example.earzo.styles.models;

import com.orm.SugarRecord;

public class Favourite extends SugarRecord {
    private String userID;
    private Product product;

    public Favourite() {
    }

    public Favourite(String userID, Product product) {
        this.userID = userID;
        this.product = product;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getUserID() {
        return userID;
    }

    public Product getProduct() {
        return product;
    }
}
