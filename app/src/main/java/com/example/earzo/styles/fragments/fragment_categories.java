package com.example.earzo.styles.fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.adapters.ListViewAdapterCategories;
import com.example.earzo.styles.models.ProductTag;

import java.util.List;

public class fragment_categories extends Fragment {


    public fragment_categories() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        //final List<Product> productList = Product.listAll(Product.class);

        ListViewAdapterCategories listAdapter = new ListViewAdapterCategories(getActivity());
        ListView listview = view.findViewById(R.id.listViewCategories);
        listview.setAdapter(listAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                String tagName = "";

                if (position == 0){
                    tagName = "Shirts";
                }else if (position == 1){
                    tagName = "Trousers";
                }else if (position == 2){
                    tagName = "Dresses";
                }else if (position == 3) {
                    tagName = "Hats";
                }

                Bundle bundle = new Bundle(2);
                bundle.putString("Selected Tag", tagName);

                Fragment newFragment = new fragment_productMenu();
                newFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        getActivity().setTitle(R.string.fragmentTitle_categories);
    }

}
