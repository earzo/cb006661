package com.example.earzo.styles.fragments;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.models.Product;
import com.squareup.picasso.Picasso;

public class fragment_itemDescription extends Fragment {

    private SwipeRefreshLayout swipeRefreshItemDescription;
    Product product;
    ImageView imageView_itemDescription_itemFullImage;


    public fragment_itemDescription() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_item_description, container, false);

        swipeRefreshItemDescription = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshItemDescription);
        swipeRefreshItemDescription.setColorSchemeResources(R.color.colorPrimary);

        swipeRefreshItemDescription.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myUpdateOperation();
            }
        });

        Long productID = getArguments().getLong("PID");
        product = Product.findById(Product.class, productID);

        TextView textView_itemDescription_itemName = (TextView) view.findViewById(R.id.textView_itemDescription_itemName);
        TextView textView_itemDescription_longDescription = (TextView) view.findViewById(R.id.textView_itemDescription_longDescription);
        imageView_itemDescription_itemFullImage = (ImageView) view.findViewById(R.id.imageView_itemDescription_itemFullImage);

        textView_itemDescription_itemName.setText(product.getName());
        textView_itemDescription_longDescription.setText(product.getLongDescription());
        Picasso.get().load(product.getFullImage()).placeholder(R.drawable.app_launcher_icon).into(imageView_itemDescription_itemFullImage);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Check if user triggered a refresh:
            case R.id.menu_refresh:

                // Signal SwipeRefreshLayout to start the progress indicator
                swipeRefreshItemDescription.setRefreshing(true);

                // Start the refresh background task.
                // This method calls setRefreshing(false) when it's finished.
                myUpdateOperation();

                return true;
        }

        // User didn't trigger a refresh, let the superclass handle this action
        return super.onOptionsItemSelected(item);

    }

    public void myUpdateOperation(){
        Picasso.get().load(product.getFullImage()).placeholder(R.drawable.app_launcher_icon).into(imageView_itemDescription_itemFullImage);
        swipeRefreshItemDescription.setRefreshing(false);
    }

}
