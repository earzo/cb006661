package com.example.earzo.styles.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.fragments.fragment_favourites;
import com.example.earzo.styles.fragments.fragment_shoppingCart;
import com.example.earzo.styles.models.Favourite;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListViewAdapterFavourites extends BaseAdapter {

    private Context mContext;

    public ListViewAdapterFavourites(Context mContext) {
        this.mContext = mContext;
    }

    public int getCount() {
        return favouritesList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @TargetApi(21)
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listView;

        if ((convertView == null) && (inflater != null)) {

            // get layout from xml file
            listView = inflater.inflate(R.layout.custom_checkout_list_item, parent, false);
        } else {
            listView = (View) convertView;
        }

        if(listView != null) {
            ConstraintLayout cl = (ConstraintLayout) listView.findViewById(R.id.constraintLayout_checkout);
            TextView textView_checkout_itemName = (TextView) listView.findViewById(R.id.textView_checkout_itemName);
            listView.findViewById(R.id.textView_checkout_itemQuantity).setVisibility(View.GONE); //no item quantity indicator for Favourites
            ImageButton imageButton_checkout_delete = (ImageButton) listView.findViewById(R.id.imageButton_checkout_delete);
            TextView textView_checkout_itemPrice = (TextView) listView.findViewById(R.id.textView_checkout_itemPrice);
            ImageView imageView_checkout = (ImageView) listView.findViewById(R.id.imageView_checkout);
            listView.findViewById(R.id.textView_checkout_addMoreItems).setVisibility(View.GONE); //no "Add More Items" link for Favourites
            String priceInString;

            ConstraintLayout outerCL = (ConstraintLayout)  listView.findViewById(R.id.outerConstraintLayout_checkout);
            //set the bottom constraint of the inner constraint layout with bottom constraint of outer constraint layout
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(outerCL);
            constraintSet.connect(cl.getId(), ConstraintSet.BOTTOM, outerCL.getId(), ConstraintSet.BOTTOM);
            constraintSet.applyTo(outerCL);

            //set the bottom margin of the inner constraint layout to 8dp, so that it'll balance the overall looks
            ConstraintLayout.LayoutParams parameter = (ConstraintLayout.LayoutParams) cl.getLayoutParams();
            parameter.setMargins(parameter.leftMargin, parameter.topMargin, parameter.rightMargin, 20);
            cl.setLayoutParams(parameter);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                cl.setBackgroundResource(R.drawable.layout_shadow);
            }

            //remove a favourite item
            imageButton_checkout_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    favouritesList.get(position).delete();
                    notifyDataSetChanged();

                    //if there are no favourites after deleting all the items, return to home
                    if(favouritesList.size() == 0){
                        Intent intent = new Intent(mContext, activity_navigationDrawer.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mContext.startActivity(intent);

                        FragmentManager manager = ((Activity)mContext).getFragmentManager();
                        manager.popBackStack();

                    }else {
                        Fragment newFragment;
                        FragmentManager manager = ((Activity) mContext).getFragmentManager();

                        manager.popBackStack();

                        FragmentTransaction transaction = manager.beginTransaction();
                        newFragment = new fragment_favourites();
                        transaction.replace(R.id.content_frame, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
            });


            priceInString = "LKR " + Double.toString(favouritesList.get(position).getProduct().getPrice());

            textView_checkout_itemName.setText(favouritesList.get(position).getProduct().getName());
            textView_checkout_itemPrice.setText(priceInString);
            Picasso.get().load(favouritesList.get(position).getProduct().getScaledImage()).placeholder(R.drawable.app_launcher_icon).into(imageView_checkout);
        }

        return listView;
    }

    private List<Favourite> favouritesList = Favourite.listAll(Favourite.class);
}
