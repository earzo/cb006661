package com.example.earzo.styles.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.adapters.ListViewAdapterShoppingCart;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.Product;
import com.example.earzo.styles.models.ShoppingCart;

import java.util.List;

public class fragment_shoppingCart extends Fragment {

    View view;
    private String UID;


    public fragment_shoppingCart() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shopping_cart, container, false);

        UserSession userSession = UserSession.getUserSession(getActivity());
        UID = userSession.getUID().toString();

        ListViewAdapterShoppingCart listAdapter = new ListViewAdapterShoppingCart(getActivity(), UID);
        ListView listview = (ListView) view.findViewById(R.id.listViewShoppingCartItems);
        listview.setAdapter(listAdapter);
        final List<ShoppingCart> shoppingCartProducts = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                Product product = Product.findById(Product.class, shoppingCartProducts.get(position).getProduct().getId());
                Long productID = product.getId();

                Bundle bundle = new Bundle(2);
                bundle.putLong("PID", productID);

                Fragment newFragment = new fragment_itemDetails();
                newFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        AppCompatButton button_shoppingCart = (AppCompatButton) view.findViewById(R.id.button_shoppingCart);

        button_shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new fragment_checkout();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        getActivity().setTitle(R.string.fragmentTitle_shoppingCart);
        List<ShoppingCart> shoppingCartProducts = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);
        if(shoppingCartProducts.size() == 0){
            Intent intent = new Intent(getActivity(), activity_navigationDrawer.class);
            startActivity(intent);

            FragmentManager manager = getActivity().getFragmentManager();
            manager.popBackStack();
        }
    }
}
