package com.example.earzo.styles.fragments;


import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.earzo.styles.R;

public class fragment_contactUs extends Fragment {

    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;


    public fragment_contactUs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);

        ConstraintLayout cl1 = (ConstraintLayout) view.findViewById(R.id.constraintLayout_calUs);
        ConstraintLayout cl = (ConstraintLayout) view.findViewById(R.id.constraintLayout_emailUs);

        cl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) ==
                PackageManager.PERMISSION_DENIED){
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                }else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:0717324674"));

                    startActivity(callIntent);
                }

            }
        });

        cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_VIEW);
                String subject = "Please help me with the below issue!";
                Uri data = Uri.parse("mailto:styles.noreply@gmail.com?subject=" + subject);
                emailIntent.setData(data);

                startActivity(emailIntent);
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],@NonNull int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:0717324674"));

                    startActivity(callIntent);
                }else{
                    Toast.makeText(getActivity(), R.string.error_permissionDenied, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getActivity().setTitle(R.string.fragmentTitle_contactUs);
    }

}
