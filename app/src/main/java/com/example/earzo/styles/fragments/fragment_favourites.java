package com.example.earzo.styles.fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.adapters.ListViewAdapterFavourites;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.Favourite;
import com.example.earzo.styles.models.PurchaseHistory;

import java.util.List;

public class fragment_favourites extends Fragment {


    public fragment_favourites() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourites, container, false);

        UserSession userSession = UserSession.getUserSession(getActivity());
        String UID = userSession.getUID().toString();

        ListViewAdapterFavourites listAdapter = new ListViewAdapterFavourites(getActivity());
        ListView listview = view.findViewById(R.id.listViewFavourites);
        listview.setAdapter(listAdapter);
        final List<Favourite> favouritesList = Favourite.find(Favourite.class, "user_ID = ?", UID);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                Bundle bundle = new Bundle(2);
                bundle.putLong("PID", favouritesList.get(position).getProduct().getId());

                android.app.Fragment newFragment = new fragment_itemDetails();
                newFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().setTitle(R.string.fragmentTitle_favourites);
    }
}