package com.example.earzo.styles.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class fragment_changeUsername extends Fragment {

    User loggedInUser;
    View view;
    String oldProfilePictureName;

    public fragment_changeUsername() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_change_username, container, false);

        final EditText edit_oldFirstName = (EditText) view.findViewById(R.id.edit_oldFirstName);
        final EditText edit_oldLastName = (EditText)  view.findViewById(R.id.edit_oldLastName);
        Button button_saveAccountChanges = (Button) view.findViewById(R.id.button_saveAccountChanges);

        UserSession userSession = UserSession.getUserSession(getActivity());
        loggedInUser = User.findById(User.class, userSession.getUID());
        oldProfilePictureName = loggedInUser.getFirstName() + "profile.jpg";
        Long UID = loggedInUser.getId();

        edit_oldFirstName.setText(loggedInUser.getFirstName());
        edit_oldLastName.setText(loggedInUser.getLastName());

        button_saveAccountChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(edit_oldFirstName.getText().toString().trim(), edit_oldLastName.getText().toString().trim());
            }
        });

        return view;
    }

    public void validate(String firstName, String lastName){

        if(loggedInUser.getFirstName().equals(firstName) && loggedInUser.getLastName().equals(lastName)){
            Toast.makeText(getActivity(), R.string.noChangesMade, Toast.LENGTH_SHORT).show();
        }else if(firstName.isEmpty() && lastName.isEmpty()){
            Toast.makeText(getActivity(), R.string.error_emptyFields, Toast.LENGTH_SHORT).show();
        }else if(firstName.isEmpty()){
            EditText edit_oldFirstName = (EditText) view.findViewById(R.id.edit_oldFirstName);
            edit_oldFirstName.requestFocus();
            edit_oldFirstName.setError(getString(R.string.error_emptyFields));
        }else if(lastName.isEmpty()){
            EditText edit_oldLastName = (EditText) view.findViewById(R.id.edit_oldLastName);
            edit_oldLastName.requestFocus();
            edit_oldLastName.setError(getString(R.string.error_emptyFields));
        }else{
            loggedInUser.setFirstName(firstName);
            loggedInUser.setLastName(lastName);
            loggedInUser.save();

            Toast.makeText(getActivity(), R.string.successful_changesSaved, Toast.LENGTH_SHORT).show();

            FragmentManager manager = getActivity().getFragmentManager();
            manager.popBackStack();

            FragmentTransaction transaction = getFragmentManager().beginTransaction().remove(this);
            getActivity().setTitle(R.string.fragmentTitle_manageAccount);
            Fragment newFragment = new fragment_manageAccount();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        getActivity().setTitle(R.string.fragmentTitle_changeUsername);
    }

}
