package com.example.earzo.styles.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;


public class fragment_manageAccount extends Fragment {

    static final int RESULT_LOAD_IMG = 1;
    private ImageView imageView_changeProfilePicture;
    private String userName;
    private String profilePictureName;


    public fragment_manageAccount() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,
            Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_account, container, false);
        imageView_changeProfilePicture = (ImageView) view.findViewById(R.id.imageView_changeProfilePicture);
        Button button_changeProfilePicture = (Button) view.findViewById(R.id.button_changeProfilePicture);
        Button button_changeUsername = (Button) view.findViewById(R.id.button_changeUsername);
        Button button_changePassword = (Button) view.findViewById(R.id.button_changePassword);

        UserSession userSession = UserSession.getUserSession(getActivity());
        User loggedInUser = User.findById(User.class, userSession.getUID());
        userName = loggedInUser.getFirstName();
        Long UID = loggedInUser.getId();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if(sp.getString("profile picture name", "profile.jpg").equals("profile.jpg")){
            profilePictureName = loggedInUser.getFirstName() + "profile.jpg";
        }else{
            profilePictureName = sp.getString("profile picture name", "profile.jpg");
        }
        loadImageFromStorage();

        button_changeProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            }
        });

        button_changeUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment;
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                getActivity().setTitle(R.string.fragmentTitle_changeUsername);
                newFragment = new fragment_changeUsername();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        button_changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment;
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                getActivity().setTitle(R.string.fragmentTitle_changePassword);
                newFragment = new fragment_changePassword();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        getActivity().setTitle(R.string.fragmentTitle_manageAccount);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                if(imageUri != null) {
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    String imageSavedPath = saveToInternalStorage(selectedImage);
                    Toast.makeText(getActivity(), R.string.successful_changesSaved, Toast.LENGTH_SHORT).show();

                    FragmentManager manager = getActivity().getFragmentManager();
                    manager.popBackStack();

                    FragmentTransaction transaction = getFragmentManager().beginTransaction().remove(this);
                    getActivity().setTitle(R.string.fragmentTitle_manageAccount);
                    Fragment newFragment = new fragment_manageAccount();
                    transaction.replace(R.id.content_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(getActivity(), "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        String profilePictureName = userName + "profile.jpg";

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        sp.edit().putString("profile picture name", profilePictureName).apply();
        File myPath = new File(directory, profilePictureName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    private void loadImageFromStorage(){
        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        try {
            File f = new File(directory.getAbsolutePath(), profilePictureName);
            if(f.exists()) {
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                imageView_changeProfilePicture.setImageBitmap(b);
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }

}
