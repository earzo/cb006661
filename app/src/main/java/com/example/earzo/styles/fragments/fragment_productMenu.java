package com.example.earzo.styles.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.adapters.GridViewAdapter;
import com.example.earzo.styles.models.Product;
import com.example.earzo.styles.models.ProductTag;

import java.util.ArrayList;
import java.util.List;

public class fragment_productMenu extends Fragment {

    private SwipeRefreshLayout swipeRefreshGrid;
    private View view;
    List<Product> productList;
    String tagName = null;

    public fragment_productMenu() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked") //The initialization of List<> productList *see below* is type safe
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_menu, container, false);

        swipeRefreshGrid = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshGrid);
        swipeRefreshGrid.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshGrid.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myUpdateOperation();
            }
        });

        Bundle bundle = getArguments();

        if(bundle != null && bundle.containsKey("Selected Tag")) {
            tagName = getArguments().getString("Selected Tag");
            getActivity().setTitle(tagName);
        }
        productList = selectRelevantProducts(tagName);

        GridViewAdapter gridAdapter = new GridViewAdapter(getActivity(), productList);
        GridView gridview = view.findViewById(R.id.gridview);
        gridview.setAdapter(gridAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                Product product = productList.get(position);
                Long productID = product.getId();

                Bundle bundle = new Bundle(2);
                bundle.putLong("PID", productID);

                Fragment newFragment = new fragment_itemDetails();
                newFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        Bundle bundle = getArguments();

        if(bundle != null && bundle.containsKey("Selected Tag")) {
            getActivity().setTitle(tagName);
        }else{
            getActivity().setTitle(R.string.fragmentTitle_Home);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Check if user triggered a refresh:
            case R.id.menu_refresh:

                // Signal SwipeRefreshLayout to start the progress indicator
                swipeRefreshGrid.setRefreshing(true);

                // Start the refresh background task.
                // This method calls setRefreshing(false) when it's finished.
                myUpdateOperation();

                return true;
        }

        // User didn't trigger a refresh, let the superclass handle this action
        return super.onOptionsItemSelected(item);

    }

    public void myUpdateOperation(){
        GridViewAdapter gridAdapter = new GridViewAdapter(getActivity(), productList);
        GridView gridview = view.findViewById(R.id.gridview);
        gridview.setAdapter(gridAdapter);
        swipeRefreshGrid.setRefreshing(false);
    }

    /*The method to create a productList for a gridView for "Categories" or "Home".
    The decision is taken by the passed tagName (only passed if a category is selected from "Categories" listView)*/
    private List selectRelevantProducts(String tag) {
        List<Product> productList = new ArrayList<>();

        if(tag == null) {
            productList = Product.listAll(Product.class);
        }else {
            List<ProductTag> productTag = ProductTag.find(ProductTag.class, "tag = ?", tag);
            Product product;
            for(ProductTag t : productTag) {
                product = t.getProduct();
                productList.add(product);
            }
        }

        return productList;
    }

    //call this method anywhere to create a crash by stackOverflow error and get a Crashlytics report
    public void stackOverflow(){
        this.stackOverflow();
    }
}
