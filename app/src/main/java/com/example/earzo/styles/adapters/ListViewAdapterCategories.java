package com.example.earzo.styles.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.earzo.styles.R;
import com.example.earzo.styles.models.Tag;

import java.util.ArrayList;
import java.util.List;

public class ListViewAdapterCategories extends BaseAdapter {

    @SuppressWarnings("unchecked") //The below initialization of List<> is type safe
    private List<Tag> tagList = getNecessaryTags();
    private Context mContext;

    public ListViewAdapterCategories(Context mContext) {
        this.mContext = mContext;
    }

    public int getCount() {
        return tagList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @TargetApi(21)
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listView;

        if ((convertView == null) && (inflater != null)) {

            // get layout from xml file
            listView = inflater.inflate(R.layout.custom_category_list_item, parent, false);
        } else {
            listView = (View) convertView;
        }

        if (listView != null) {
            ConstraintLayout cl = (ConstraintLayout) listView.findViewById(R.id.constraintLayout_category);
            TextView textView_listItem_categoryName = (TextView) listView.findViewById(R.id.textView_listItem_categoryName);
            ImageView imageView_listView_categoryImage = (ImageView) listView.findViewById(R.id.imageView_listView_categoryImage);
            imageView_listView_categoryImage.setClipToOutline(true);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                cl.setBackgroundResource(R.drawable.layout_shadow);
            }

            if (position == 0) {
                imageView_listView_categoryImage.setImageResource(R.drawable.shirt);
            } else if (position == 1) {
                imageView_listView_categoryImage.setImageResource(R.drawable.trousers);
            } else if (position == 2) {
                imageView_listView_categoryImage.setImageResource(R.drawable.dress);
            } else if (position == 3) {
                imageView_listView_categoryImage.setImageResource(R.drawable.hat);
            }

            textView_listItem_categoryName.setText(tagList.get(position).getTag());
        }

        return listView;
    }


    private List getNecessaryTags(){
        List<Tag> shirt = Tag.find(Tag.class, "tag = ?", "Shirts");
        List<Tag> trousers = Tag.find(Tag.class, "tag = ?", "Trousers");
        List<Tag> dress = Tag.find(Tag.class, "tag = ?", "Dresses");
        List<Tag> hat = Tag.find(Tag.class, "tag = ?", "Hats");

        Tag shirtCategory = shirt.get(0);
        Tag trousersCategory = trousers.get(0);
        Tag dressCategory = dress.get(0);
        Tag hatCategory = hat.get(0);

        List<Tag> tagList = new ArrayList<>();

        tagList.add(shirtCategory);
        tagList.add(trousersCategory);
        tagList.add(dressCategory);
        tagList.add(hatCategory);

        return tagList;
    }

}
