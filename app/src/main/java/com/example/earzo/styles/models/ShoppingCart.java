package com.example.earzo.styles.models;

import com.orm.SugarRecord;

public class ShoppingCart extends SugarRecord {
    private String userID;
    private Product product;
    private int quantity;

    public ShoppingCart() {
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUserID() {
        return userID;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }
}
