package com.example.earzo.styles.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.AppCompatButton;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.models.Favourite;
import com.example.earzo.styles.models.Product;
import com.example.earzo.styles.models.ShoppingCart;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class fragment_itemDetails extends Fragment {

    private SwipeRefreshLayout swipeRefreshItemDetails;
    private String UID;
    private Product product;
    private ConstraintLayout cl2;
    private ImageView imageView;

    public fragment_itemDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_item_details, container, false);

        swipeRefreshItemDetails = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshItemDetails);
        swipeRefreshItemDetails.setColorSchemeResources(R.color.colorPrimary);

        swipeRefreshItemDetails.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myUpdateOperation();
            }
        });


        final Long productID = getArguments().getLong("PID");
        product = Product.findById(Product.class, productID);

        UserSession userSession = UserSession.getUserSession(getActivity());
        UID = userSession.getUID().toString();
        final List<ShoppingCart> shoppingCart = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);

        Drawable fabIconSource = getResources().getDrawable(R.drawable.ic_menu_share);
        Drawable changeIconColor = fabIconSource.getConstantState().newDrawable();
        changeIconColor.mutate().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        fab.setImageDrawable(changeIconColor);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.get().load(product.getFullImage()).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("image/*");
                        share.putExtra(Intent.EXTRA_SUBJECT, "A product from STYLE OMEGA™!");
                        share.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                        share.putExtra(Intent.EXTRA_TEXT, "Hey checkout this product from Style Omega™!\n\n" + product.getFullImage());

                        startActivity(Intent.createChooser(share, "Share Via:"));
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
            }
        });

        ConstraintLayout outerCL = (ConstraintLayout) view.findViewById(R.id.outerConstraintLayout);
        ConstraintLayout cl = (ConstraintLayout) view.findViewById(R.id.constraintLayout);
        cl2 = (ConstraintLayout) view.findViewById(R.id.constraintLayout2);
        ConstraintLayout cl3 = (ConstraintLayout) view.findViewById(R.id.constraintLayout3);
        imageView = (ImageView) view.findViewById(R.id.imageView_purchased);
        TextView textView_itemDetails_itemName = (TextView) view.findViewById(R.id.textView_itemDetails_itemName);
        TextView textView_itemDetails_itemPrice = (TextView) view.findViewById(R.id.textView_itemDetails_itemPrice);
        TextView textView_itemDetails_itemDescription = (TextView) view.findViewById(R.id.textView_itemDetails_itemDescription);
        TextView textView_itemDetails_itemQuantity = (TextView) view.findViewById(R.id.textView_itemDetails_itemQuantity);
        TextView textView_itemDetails_itemID = (TextView) view.findViewById(R.id.textView_itemDetails_itemID);
        TextView textView_itemDetails_readMore = (TextView) view.findViewById(R.id.textView_itemDetails_readMore);
        AppCompatButton button_itemDetails_buy = (AppCompatButton) view.findViewById(R.id.button_itemDetails_buy);
        Button button_itemDetails_addToCart = (Button) view.findViewById(R.id.button_itemDetails_addToCart);
        Button button_itemDetails_addToFavourites = (Button) view.findViewById(R.id.button_itemDetails_addToFavourites);

        button_itemDetails_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if cart has this item available, remove that item from the cart
                for (ShoppingCart cartItem : shoppingCart) {
                    if(cartItem.getProduct().getId() == product.getId()) {
                        cartItem.delete();
                    }
                }
                final NumberPicker picker = new NumberPicker(getActivity());
                picker.setMinValue(1);
                picker.setMaxValue(product.getQuantity());

                FrameLayout layout = new FrameLayout(getActivity());
                layout.addView(picker, new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER
                ));

                new AlertDialog.Builder(getActivity()).setView(layout).setTitle(R.string.dialogTitle).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle(2);
                        bundle.putLong("PID", productID);
                        int value = picker.getValue();
                        bundle.putInt("Quantity", value);

                        Fragment newFragment;
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        getActivity().setTitle(R.string.fragmentTitle_checkout);
                        newFragment = new fragment_checkout();
                        newFragment.setArguments(bundle);
                        transaction.replace(R.id.content_frame, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }).setNegativeButton(android.R.string.cancel, null).show();
            }
        });

        // if this item is already in the shopping cart,
        // change the name to "VIEW ITEM IN CART" and functionality to open the shopping cart, of the "BUY IT NOW" button
        if (isAvailableInShoppingCart() == null) {

            button_itemDetails_addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final NumberPicker picker = new NumberPicker(getActivity());
                    picker.setMinValue(1);
                    picker.setMaxValue(product.getQuantity());

                    FrameLayout layout = new FrameLayout(getActivity());
                    layout.addView(picker, new FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.WRAP_CONTENT,
                            Gravity.CENTER
                    ));

                    new AlertDialog.Builder(getActivity()).setView(layout).setTitle(R.string.dialogTitle).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ShoppingCart newShoppingCart = new ShoppingCart();
                            newShoppingCart.setUserID(UID);
                            newShoppingCart.setProduct(product);
                            newShoppingCart.setQuantity(picker.getValue());
                            newShoppingCart.save();

                            Bundle bundle = new Bundle(2);
                            bundle.putLong("PID", productID);


                            Fragment newFragment;
                            FragmentManager manager = getActivity().getFragmentManager();

                            manager.popBackStack();

                            FragmentTransaction transaction = manager.beginTransaction();
                            newFragment = new fragment_itemDetails();
                            newFragment.setArguments(bundle);
                            transaction.replace(R.id.content_frame, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                            Toast.makeText(getActivity(), R.string.successful_addToCart, Toast.LENGTH_SHORT).show();
                        }
                    }).setNegativeButton(android.R.string.cancel, null).show();
                }
            });
        }else{

            button_itemDetails_addToCart.setText(R.string.fragmentElement_button_itemAlreadyInCart);

            button_itemDetails_addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment newFragment;
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    getActivity().setTitle(R.string.fragmentTitle_cart);
                    newFragment = new fragment_shoppingCart();
                    transaction.replace(R.id.content_frame, newFragment);
                    transaction.addToBackStack("shoppingCart");
                    transaction.commit();
                }
            });
        }

        if(isAvailableInFavourites() == null){
            button_itemDetails_addToFavourites.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Favourite newFavourite = new Favourite();
                    newFavourite.setUserID(UID);
                    newFavourite.setProduct(product);
                    newFavourite.save();

                    Bundle bundle = new Bundle(2);
                    bundle.putLong("PID", productID);


                    Fragment newFragment;
                    FragmentManager manager = getActivity().getFragmentManager();

                    manager.popBackStack();

                    FragmentTransaction transaction = manager.beginTransaction();
                    newFragment = new fragment_itemDetails();
                    newFragment.setArguments(bundle);
                    transaction.replace(R.id.content_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    Toast.makeText(getActivity(), R.string.successful_addToFavourite, Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            button_itemDetails_addToFavourites.setText(R.string.fragmentElement_button_itemAlreadyInFavourites);
            button_itemDetails_addToFavourites.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment newFragment;
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    getActivity().setTitle(R.string.fragmentTitle_favourites);
                    newFragment = new fragment_favourites();
                    transaction.replace(R.id.content_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }

        String price = "LKR " + Double.toString(product.getPrice());
        String ID = product.getId().toString();
        String quantity;

        //if the product is NOT available, remove the "BUY IT NOW" and "ADD TO CART" buttons from the layout and make the Quantity field as "Unavailable"
        if (!product.isActive()) {
            button_itemDetails_buy.setVisibility(View.GONE); //remove "BUY IT NOW" button
            button_itemDetails_addToCart.setVisibility(View.GONE); //remove "ADD TO CART" button

            //set the top constraint of the "ADD TO FAVOURITES" button with bottom constraint of an inner constraint layout above the button
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(outerCL);
            constraintSet.connect(button_itemDetails_addToFavourites.getId(), ConstraintSet.TOP, cl.getId(), ConstraintSet.BOTTOM);
            constraintSet.applyTo(outerCL);

            //set the top margin of the "ADD TO FAVOURITES" button to 50dp, so that it'll balance the overall looks
            ConstraintLayout.LayoutParams parameter = (ConstraintLayout.LayoutParams) button_itemDetails_addToFavourites.getLayoutParams();
            parameter.setMargins(parameter.leftMargin, 50, parameter.rightMargin, parameter.bottomMargin);
            button_itemDetails_addToFavourites.setLayoutParams(parameter);

            quantity = "Unavailable"; //make the Quantity field as "Unavailable"
            textView_itemDetails_itemQuantity.setTextColor(Color.parseColor("#FF0000")); //change the color of the Quantity field to red
        } else {
            quantity = Integer.toString(product.getQuantity());
        }

        textView_itemDetails_itemName.setText(product.getName());
        textView_itemDetails_itemPrice.setText(price);
        textView_itemDetails_itemQuantity.setText(quantity);
        textView_itemDetails_itemID.setText(ID);
        textView_itemDetails_itemDescription.setText(product.getShortDescription());

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            cl.setBackgroundResource(R.drawable.layout_shadow);
            cl3.setBackgroundResource(R.drawable.layout_shadow);
        }

        loadImage();

        textView_itemDetails_readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle(2);
                bundle.putLong("PID", productID);

                Fragment newFragment;
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                newFragment = new fragment_itemDescription();
                newFragment.setArguments(bundle);
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().setTitle(R.string.app_name);
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;

        try {
            File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bmpUri;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Check if user triggered a refresh:
            case R.id.menu_refresh:

                // Signal SwipeRefreshLayout to start the progress indicator
                swipeRefreshItemDetails.setRefreshing(true);

                // Start the refresh background task.
                // This method calls setRefreshing(false) when it's finished.
                myUpdateOperation();

                return true;
        }

        // User didn't trigger a refresh, let the superclass handle this action
        return super.onOptionsItemSelected(item);

    }

    public void myUpdateOperation() {
        loadImage();
        swipeRefreshItemDetails.setRefreshing(false);
    }

    public void loadImage() {
        int imageDimension = (int) getResources().getDimension(R.dimen.image_size);

        Picasso.get().load(product.getScaledImage()).resize(imageDimension, imageDimension).centerCrop().placeholder(R.drawable.app_launcher_icon).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                assert imageView != null;
                imageView.setImageBitmap(bitmap);
                Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated(@NonNull Palette palette) {
                        Palette.Swatch textSwatch = palette.getDominantSwatch();

                        if (textSwatch == null) {
                            cl2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        } else {
                            cl2.setBackgroundColor(textSwatch.getRgb());
                        }
                    }
                });
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    //method to check if an item is available in shopping cart
    public Long isAvailableInShoppingCart() {
        List<ShoppingCart> shoppingCart = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);

        for (ShoppingCart currentCart : shoppingCart) {
            if (currentCart.getProduct().getId().equals(product.getId())) {
                return currentCart.getProduct().getId();
            }
        }

        return null;
    }

    //method to check if an item is a favourite item
    public Long isAvailableInFavourites(){
        List<Favourite> favourites = Favourite.find(Favourite.class, "user_ID = ?", UID);

        for (Favourite currentFavourite : favourites) {
            if (currentFavourite.getProduct().getId().equals(product.getId())) {
                return currentFavourite.getProduct().getId();
            }
        }

        return null;
    }
}
