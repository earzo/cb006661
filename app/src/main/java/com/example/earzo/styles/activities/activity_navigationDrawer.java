package com.example.earzo.styles.activities;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.extra.UserSession;
import com.example.earzo.styles.fragments.fragment_categories;
import com.example.earzo.styles.fragments.fragment_contactUs;
import com.example.earzo.styles.fragments.fragment_manageAccount;
import com.example.earzo.styles.fragments.fragment_favourites;
import com.example.earzo.styles.fragments.fragment_productMenu;
import com.example.earzo.styles.fragments.fragment_purchasedItems;
import com.example.earzo.styles.fragments.fragment_shoppingCart;
import com.example.earzo.styles.models.Favourite;
import com.example.earzo.styles.models.PurchaseHistory;
import com.example.earzo.styles.models.ShoppingCart;
import com.example.earzo.styles.models.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class activity_navigationDrawer extends activity_base
        implements NavigationView.OnNavigationItemSelectedListener {

    String UID;
    ImageView navHeaderProfileImage;
    String profilePictureName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        UserSession userSession = UserSession.getUserSession(this);

        User LoggedInUser = User.findById(User.class, userSession.getUID());
        UID = LoggedInUser.getId().toString();

        View headerView = navigationView.getHeaderView(0);
        TextView navHeaderUserName = (TextView) headerView.findViewById(R.id.textView_loggedUser);
        String user = LoggedInUser.getFirstName() + " " + LoggedInUser.getLastName();
        navHeaderUserName.setText(user);

        TextView navHeaderUserEmail = (TextView) headerView.findViewById(R.id.textView_loggedUserEmail);
        navHeaderUserEmail.setText(LoggedInUser.getUserEmail());


        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        if(sp.getString("profile picture name", "profile.jpg").equals("profile.jpg")){
            profilePictureName = LoggedInUser.getFirstName() + "profile.jpg";
        }else{
            profilePictureName = sp.getString("profile picture name", "profile.jpg");
        }

        navHeaderProfileImage = (ImageView) headerView.findViewById(R.id.imageViewProfilePicture);
        loadImageFromStorage();

        int position = getIntent().getIntExtra("position", R.id.content_frame);

        if(getFragmentManager().findFragmentById(R.id.content_frame) == null) {
            if(position == R.id.content_frame) {
                setTitle("Home");
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, new fragment_productMenu());
                transaction.commit();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        setTitle(R.string.app_name);
        loadImageFromStorage();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            setTitle(R.string.fragmentTitle_manageAccount);
            Fragment newFragment = new fragment_manageAccount();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        }else if (id == R.id.action_logout){

            UserSession userSession = UserSession.getUserSession(this);
            userSession.logout();
            Intent intent = new Intent(this, activity_login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment newFragment;
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        if (id == R.id.nav_home) {
            setTitle(R.string.fragmentTitle_Home);
            newFragment = new fragment_productMenu();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if(id == R.id.nav_categories){
            setTitle(R.string.fragmentTitle_categories);
            newFragment = new fragment_categories();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_shoppingCart) {
            List<ShoppingCart> shoppingCartProducts = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);

            //if shopping cart is empty
            if(shoppingCartProducts.size() == 0){
                Toast.makeText(this, R.string.emptyCart, Toast.LENGTH_SHORT).show();
            }else {
                setTitle(R.string.fragmentTitle_cart);
                newFragment = new fragment_shoppingCart();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack("shoppingCart");
                transaction.commit();
            }

        } else if (id == R.id.nav_favourites) {
            List<Favourite> favouriteProducts = Favourite.find(Favourite.class, "user_ID = ?", UID);

            //if no favourite items
            if(favouriteProducts.size() == 0){
                Toast.makeText(this, R.string.emptyFavourites, Toast.LENGTH_SHORT).show();
            }else {
                setTitle(R.string.fragmentTitle_favourites);
                newFragment = new fragment_favourites();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }

        } else if (id == R.id.nav_purchaseHistory) {
            List<PurchaseHistory> purchasedProducts = PurchaseHistory.find(PurchaseHistory.class, "user_ID = ?", UID);

            //if no items purchased
            if(purchasedProducts.size() == 0){
                Toast.makeText(this, R.string.emptyPurchases, Toast.LENGTH_SHORT).show();
            }else {
                setTitle(R.string.fragmentTitle_purchaseHistory);
                newFragment = new fragment_purchasedItems();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        } else if (id == R.id.nav_contactUs) {
            setTitle(R.string.fragmentTitle_contactUs);
            newFragment = new fragment_contactUs();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadImageFromStorage(){
        ContextWrapper cw = new ContextWrapper(this);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        try {
            File f = new File(directory.getAbsolutePath(), profilePictureName);
            if(f.exists()) {
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                navHeaderProfileImage.setImageBitmap(b);
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }
}
