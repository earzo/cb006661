package com.example.earzo.styles.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.activities.activity_navigationDrawer;
import com.example.earzo.styles.fragments.fragment_shoppingCart;
import com.example.earzo.styles.models.ShoppingCart;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListViewAdapterShoppingCart extends BaseAdapter {

    private Context mContext;
    private List<ShoppingCart> shoppingList;

    public ListViewAdapterShoppingCart(Context mContext, String UID) {
        this.mContext = mContext;
        shoppingList = ShoppingCart.find(ShoppingCart.class, "user_ID = ?", UID);
    }

    public int getCount() {
        return shoppingList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @TargetApi(21)
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listView;

        if ((convertView == null) && (inflater != null)) {

            // get layout from xml file
            listView = inflater.inflate(R.layout.custom_checkout_list_item, parent, false);
        } else {
            listView = (View) convertView;
        }

        if(listView != null) {
            ConstraintLayout cl = (ConstraintLayout) listView.findViewById(R.id.constraintLayout_checkout);
            TextView textView_checkout_itemName = (TextView) listView.findViewById(R.id.textView_checkout_itemName);
            TextView textView_checkout_itemPrice = (TextView) listView.findViewById(R.id.textView_checkout_itemPrice);
            TextView textView_checkout_itemQuantity = (TextView) listView.findViewById(R.id.textView_checkout_itemQuantity);
            TextView textView_checkout_addMoreItems = (TextView) listView.findViewById(R.id.textView_checkout_addMoreItems);
            ImageView imageView_checkout = (ImageView) listView.findViewById(R.id.imageView_checkout);
            ImageButton imageButton_checkout_delete = (ImageButton) listView.findViewById(R.id.imageButton_checkout_delete);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                cl.setBackgroundResource(R.drawable.layout_shadow);
            }

            final int quantity = shoppingList.get(position).getQuantity();
            String priceInString, quantityInString;


            //to check if the number of current item in the cart is equal to the actual quantity of the product available
            if(shoppingList.get(position).getQuantity() == shoppingList.get(position).getProduct().getQuantity()) {
                textView_checkout_addMoreItems.setVisibility(View.GONE); //remove this option to add another item

                ConstraintLayout outerCL = (ConstraintLayout)  listView.findViewById(R.id.outerConstraintLayout_checkout);
                //set the bottom constraint of the inner constraint layout with bottom constraint of outer constraint layout
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(outerCL);
                constraintSet.connect(cl.getId(), ConstraintSet.BOTTOM, outerCL.getId(), ConstraintSet.BOTTOM);
                constraintSet.applyTo(outerCL);

                //set the bottom margin of the inner constraint layout to 8dp, so that it'll balance the overall looks
                ConstraintLayout.LayoutParams parameter = (ConstraintLayout.LayoutParams) cl.getLayoutParams();
                parameter.setMargins(parameter.leftMargin, parameter.topMargin, parameter.rightMargin, 20);
                cl.setLayoutParams(parameter);
            }else{
                //set a click listener to add some more items
                textView_checkout_addMoreItems.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final NumberPicker picker = new NumberPicker(mContext);
                        picker.setMinValue(1);
                        picker.setMaxValue(shoppingList.get(position).getProduct().getQuantity() - quantity); //the max value for the picker should be (available quantity - quantity in cart)

                        FrameLayout layout = new FrameLayout(mContext);
                        layout.addView(picker, new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER
                        ));

                        new AlertDialog.Builder(mContext).setView(layout).setTitle(R.string.dialogTitle).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                shoppingList.get(position).setQuantity(shoppingList.get(position).getQuantity() + picker.getValue());
                                shoppingList.get(position).save();
                                notifyDataSetChanged();

                                Fragment newFragment;
                                FragmentManager manager = ((Activity) mContext).getFragmentManager();

                                manager.popBackStack("shoppingCart", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                                FragmentTransaction transaction = manager.beginTransaction();
                                newFragment = new fragment_shoppingCart();
                                transaction.replace(R.id.content_frame, newFragment);
                                transaction.addToBackStack("shoppingCart");
                                transaction.commit();
                                Toast.makeText(mContext, R.string.successful_cartUpdated, Toast.LENGTH_SHORT).show();
                            }
                        }).setNegativeButton(android.R.string.cancel, null).show();
                    }
                });
            }


            imageButton_checkout_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shoppingList.get(position).delete();
                    notifyDataSetChanged();

                    //if the shopping cart is empty after deleting all the items, return to home
                    if(shoppingList.size() == 0){
                        Intent intent = new Intent(mContext, activity_navigationDrawer.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mContext.startActivity(intent);

                        FragmentManager manager = ((Activity)mContext).getFragmentManager();
                        manager.popBackStack();

                    }else {
                        Fragment newFragment;
                        FragmentManager manager = ((Activity) mContext).getFragmentManager();

                        manager.popBackStack("shoppingCart", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        FragmentTransaction transaction = manager.beginTransaction();
                        newFragment = new fragment_shoppingCart();
                        transaction.replace(R.id.content_frame, newFragment);
                        transaction.addToBackStack("shoppingCart");
                        transaction.commit();
                    }
                }
            });

            priceInString = "LKR " + Double.toString(shoppingList.get(position).getProduct().getPrice());
            quantityInString = "Quantity " + Integer.toString(quantity);

            textView_checkout_itemName.setText(shoppingList.get(position).getProduct().getName());
            textView_checkout_itemPrice.setText(priceInString);
            textView_checkout_itemQuantity.setText(quantityInString);
            Picasso.get().load(shoppingList.get(position).getProduct().getScaledImage()).placeholder(R.drawable.app_launcher_icon).into(imageView_checkout);
        }

        return listView;
    }
}
