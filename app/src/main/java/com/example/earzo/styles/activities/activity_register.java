package com.example.earzo.styles.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.earzo.styles.R;
import com.example.earzo.styles.extra.MailSender;
import com.example.earzo.styles.models.User;

import java.util.List;

public class activity_register extends activity_base {

    private String email;
    private Button button_register;
    private EditText editTextNewEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextNewEmail = (EditText) findViewById(R.id.editText_newUserEmail);
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        button_register = (Button) findViewById(R.id.button_register);
        button_register.setEnabled(false);

        //checking if the email format is correct
        editTextNewEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                button_register.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.matches(emailPattern) && s.length() > 0) {
                    button_register.setEnabled(true);
                    email = text;
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

        setTitle(R.string.app_name);
    }

    /** Called when the user taps the Register button */
    public void openLoginScreen(View view) {
        EditText editTextNewFirstName = (EditText) findViewById(R.id.editText_newFirstName);
        EditText editTextNewLastName = (EditText) findViewById(R.id.editText_newLastName);
        EditText editTextNewPW = (EditText) findViewById(R.id.editText_newPassword);
        EditText editTextConfirmNewPW = (EditText) findViewById(R.id.editText__confirmNewPassword);

        String firstName = editTextNewFirstName.getText().toString().trim();
        String lastName = editTextNewLastName.getText().toString().trim();
        String newPW = editTextNewPW.getText().toString();
        String confirmPW = editTextConfirmNewPW.getText().toString();

        validateInputs(new EditText[] {editTextNewFirstName, editTextNewLastName, editTextNewEmail, editTextNewPW, editTextConfirmNewPW}, firstName, lastName, email, newPW, confirmPW);
    }


    public void validateInputs(EditText[] userDetails, String firstName, String lastName, final String email, String newPW, String confirmPW){

        List<User> userList = User.find(User.class, "user_email = ? or first_name = ? or last_name = ?", email, firstName, lastName);
        int count = 0;

        for(EditText currentDetail : userDetails){
            if(currentDetail.getText().toString().trim().isEmpty()){
                currentDetail.requestFocus();
                currentDetail.setError(getString(R.string.error_emptyFields));
                count++;
            }
        }

        if(count == 0) {
            if (userList.size() == 1) {
                Toast.makeText(this, R.string.error_existingUser, Toast.LENGTH_SHORT).show();
            } else if (newPW.equals(confirmPW)) {
                User user = new User(email, firstName, lastName, newPW);
                user.save();

                Intent intent = new Intent(this, activity_login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                final String body = "Hi " + firstName + " " + lastName + "," + "\n\nCHEERS! You just became an official member of Style Omega™ team!\n\nWe have a wide collection of HIGH-QUALITY wearables available just for you! Hope you enjoy your visits with us." + "\n\n________________________________________________________\nTeam Style Omega™";

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            MailSender sender = new MailSender("styles.noreply@gmail.com", "#StyleS@10#");
                            sender.sendMail("WELCOME TO Style Omega™!",
                                    body,
                                    "styles.noreply@gmail.com",
                                    email);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                this.finish();
            } else {
                Toast.makeText(this, R.string.error_PWunmatch, Toast.LENGTH_SHORT).show();
            }

            count = 0;
        }
    }
}
