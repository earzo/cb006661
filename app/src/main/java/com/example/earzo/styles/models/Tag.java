package com.example.earzo.styles.models;

import com.orm.SugarRecord;

public class Tag extends SugarRecord {
    private String tag;

    public Tag() {
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
